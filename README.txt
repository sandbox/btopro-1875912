This module gives you a link on the display of most entities
(nodes, users, taxonomy terms, and field collections) which lets
you update the value of associated date fields to the current time.
This is useful for marking things as complete and triggering other
remote updates / Rules to fire based on the "approval" (timestamp)
given to a field. Works with multiple date fields (single value) as
well as all three date field types provided by the Date module.

<h3>To use</h3>
<ul>
  <li>Download Datelink, Date, and Entity modules</li>
  <li>Enable Datelink, Date, Entity, and Field modules.</li>
  <li>Add a Date field to any content type (or entity like node, user, etc)</li>
  <li>Create a new instance of that entity</li>
  <li>You should now see a link at the bottom of the content for that entity allowing you to update the date.</li>
</ul>

Grant permission to other user roles that you'd like to be able to
perform this operation.

Date link is btopro's submission for Module Off #1 (http://moduleoff.com/).