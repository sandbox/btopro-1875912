// JavaScript Document
(function ($) {
  //extend the drupal js object by adding in datelink name-space
  Drupal.datelink = Drupal.datelink || { functions: {} };
  // standard function to set result via AJAX
  Drupal.datelink.ajax_call = function(field) {
    // store datelink settings for shorter calls
    var settings = Drupal.settings.datelink;
    // form address callback starting with ajax path
    var address = Drupal.settings.basePath + settings.ajaxPath + '/';
    // append token for secure transaction
    address += settings.token + '/';
    // append entity type
    address += settings.entityType + '/';
    // append entity id
    address += settings.etid + '/';
    // append field
    address += field;
    // make standard ajax call
    $.ajax({
      type: "POST",
      url: address,
      success: function(msg) {
        // display response message
        if (msg) {
          alert(Drupal.t('Date has been set to the current time.'));
        }
        else {
          alert(Drupal.t('You do not have access to update this field.'));  
        }
      }
    });
  }
  // Apply click handler on document load
  $(document).ready(function() {
    $('a.datelink').click(function(){
      // extract field name from id
      var fieldName = $(this).attr('id').replace('datelink-fieldname-', '');
      // perform ajax request based on field clicked
      Drupal.datelink.ajax_call(fieldName);
    });
  });
})(jQuery);